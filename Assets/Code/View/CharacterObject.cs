﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class CharacterObject : MonoBehaviour {

	#region private fields

	private bool 		_bVisible = false;
	
	// Components
	private MeshFilter 		_filter;
	private Mesh			_mesh;
	private MeshRenderer 	_renderer;
	
	// Drawing
	private Vector3[] 	_vertices;
	private Vector2[] 	_uvs;
	private int[] 		_triangles;
	
	#endregion
	
	#region public methods
	
	public void Init(bool bDrawImmediate ) {
		
		_filter = GetComponent<MeshFilter> ();
		_renderer = GetComponent<MeshRenderer> ();
		_mesh = new Mesh();
		
		_vertices = new Vector3[4]{	new Vector3(0, 0, 0),
			new Vector3(0, 0, GameDefs.kTileSize),
			new Vector3(GameDefs.kTileSize, 0, GameDefs.kTileSize),
			new Vector3(GameDefs.kTileSize, 0, 0)};
		
		_triangles = new int[]{	0, 1, 3,
			1, 2, 3};
		
		_uvs = new Vector2[4]{	new Vector2(0, 0),
			new Vector2(0, 1),
			new Vector2(1, 1),
			new Vector2(1, 0)};
		
		if (bDrawImmediate)
			Draw ();
	}
	
	public void Draw() {
		
		if (!_bVisible) {
			_mesh = new Mesh();
			_mesh.name = "newMesh";
			
			_mesh.vertices = _vertices;
			_mesh.triangles = _triangles;
			_mesh.uv = _uvs;
			
			_filter.mesh = _mesh;
			
			Material mat = Resources.Load(ResourceDefs.kTile_Materials_Living + "Player") as Material;
			
			if(!mat)
				throw new UnityException(this + ": no tile material for type: " + "Player");
			else
				_renderer.material = mat;
			
			_bVisible = true;
		}
	}
	
	#endregion
}
