﻿using UnityEngine;
using System.Collections;

public static class ResourceDefs
{
	public static string kTile_Materials 				= "Materials/Tile/";
	public static string kTile_Materials_Environment 	= "Materials/Tile/Environment/";
	public static string kTile_Materials_Living 		= "Materials/Tile/Living/";
}
