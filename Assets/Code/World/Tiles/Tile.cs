﻿using UnityEngine;
using System.Collections;

public enum TileType {
	Default,
	EdgeOfTheWOrld,

	Count
}

namespace WeeRPG.World {

	public class Tile {

		public TileType type { get; private set; }
		public Location location { get; private set; }

		public Tile() {
			type = TileType.Default;	
		}
	}
}
