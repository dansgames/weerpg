﻿using UnityEngine;
using System.Collections;

using WeeRPG.World;

namespace WeeRPG.Tools {

	public static class WorldTools
	{
		public static Vector3 GetWorldPositionForTileLocation (Location location) {

			float posX = (GameDefs.kTileSize * (location.longitude-1));
			float posZ = (GameDefs.kTileSize * (location.lattitude-1));

			return new Vector3(posX,0,posZ);
		}
	}
}