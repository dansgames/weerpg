﻿using UnityEngine;
using System.Collections;

public enum CharacterState {
	Idle,
	Dead
}

namespace WeeRPG.AI {

	public class Character
	{
		#region private fields

		// My Bits
		private Memory 	_memory;
		private Brain 	_brain;

		// Current State
		private CharacterState _state;

		private string _firstName = "Hubert";
		private string _secondName = "Pinkerwinkle";

		private Location _location;

		private int _worldMaxX;
		private int _worldMaxZ;

		#endregion

		// Constructor
		//TODO: CONFIGS
		public Character(/* CONFIG*/) {
			
		}

		public Character(int worldMaxX, int worldMaxZ, Vector2 startPos) {
			_worldMaxX = worldMaxX;
			_worldMaxZ = worldMaxZ;
			_location = new Location(5, 5);

			_state = CharacterState.Idle;
		}

		#region public interface

		#region Public properties
		
		public string firstName 	{ get { return _firstName; } }
		public string secondName 	{ get { return _secondName; } }
		public Location location 	{ get { return _location; } }
		public CharacterState state { get { return _state; } }
		
		#endregion

		#region public methods

		public string ReturnStatusAsString() {
			return 	"<color=blue>" +_firstName + "</color>" +
					" @ <color=white>[" + _location + "]</color>" +
					" is <color=green>" + _state + "</color>";
		}

		#endregion

		#endregion
	}
}
