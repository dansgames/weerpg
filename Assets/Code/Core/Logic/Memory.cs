﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using WeeRPG.World;

namespace WeeRPG.AI {

	public class Memory {

		//TODO: Create Tile Information Class for Memories
		private Dictionary<Location, Tile> _tileMemories = new Dictionary<Location, Tile>();

		public void AddMemoryOfTile(Location location, Tile tile) {
			if (_tileMemories.ContainsKey (location))
				_tileMemories [location] = tile;
			else
				_tileMemories.Add (location, tile);
		}
	}
}
