﻿using UnityEngine;
using System.Collections;

namespace WeeRPG.Core.States
{
	public interface IUpdateable 
	{
		void Update();
		void LateUpdate();
		void FixedUpdate();
	}
}