﻿using UnityEngine;
using System.Collections;

namespace WeeRPG.Core.States
{
	public interface IState
	{
		void OnEnter();
		void OnExit();
		bool CanTransition();
		IState NextState();
	}
}