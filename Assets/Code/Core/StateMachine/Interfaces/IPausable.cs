﻿using UnityEngine;
using System.Collections;

namespace WeeRPG.Core.States
{
	public interface IPausable 
	{
		void ApplicationHasGoneIntoBackground();
	}
}