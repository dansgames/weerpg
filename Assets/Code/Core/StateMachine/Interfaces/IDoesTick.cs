﻿using UnityEngine;
using System.Collections;

namespace WeeRPG.Core.States
{
	public interface IDoesTick
	{
		void OnTick();
	}
}