﻿using UnityEngine;
using System.Collections;

namespace WeeRPG.Core.States
{
	public class BaseState : IState {

		#region private fields
		private IState _nextState;
		#endregion

		virtual public void OnEnter() {
			Debug.Log ("OnEnter() BaseState");
		}

		virtual public void OnExit() {
			
		}

		virtual public IState NextState() {
			return _nextState;
		}

		virtual public bool CanTransition() {
			return _nextState != default(IState);
		}
	}
}
