﻿using UnityEngine;
using System.Collections;

using WeeRPG.AI;
using WeeRPG.Tools;

namespace WeeRPG.Core.States
{
	public sealed class GameState : BaseState, IPausable, IDoesTick
	{
		#region private fields

		private Character _player;
		private CharacterObject _playerCharacterObject;

		#endregion

		public GameState(Character character) {
			_player = character;
		}

		#region IState Methods

		override public void OnEnter() {
			Debug.Log ("OnEnter() GameState");

			GameObject playerObj = new GameObject ("Player");
			_playerCharacterObject = playerObj.AddComponent<CharacterObject> ();
			Vector3 playerWorldPos = WorldTools.GetWorldPositionForTileLocation (_player.location);
			playerWorldPos.y = 1;
			_playerCharacterObject.transform.position = playerWorldPos;
			_playerCharacterObject.Init (true);
		}

		#endregion

		#region ITick methods

		public void OnTick() {
			switch (_player.state) {
			case CharacterState.Idle:

				Debug.Log (_player.ReturnStatusAsString());


				break;

			case CharacterState.Dead:

				break;
			}
		}

		#endregion

		#region IPausable methods

		public void ApplicationHasGoneIntoBackground() {
			
		}

		#endregion
	}
}
