﻿using UnityEngine;
using System.Collections;

using WeeRPG.World;
using WeeRPG.View;

namespace WeeRPG.Core {
	public class WorldBuilder : MonoBehaviour {

		#region private fields

		private Vector2 _worldSize;
		private Tile[,] _worldTiles;

		private Transform _world;

		#endregion

		#region public methods

		/// <summary>
		/// Runs random test build world.
		/// </summary>
		/// <param name="width">Width.</param>
		/// <param name="height">Height.</param>
		public void RunRandomTest (int width, int height) {
			if (DebugDefs.kDebugWOrldBuilder)
				Debug.Log ("RunRandomTest()");

			_world = new GameObject ("_world").transform;

			_worldSize = new Vector2 (width, height);
			
			_worldTiles = new Tile[width, height];
			
			GenerateWorldModel ();
			CreateTileGameObjects ();
		}

		/// <summary>
		/// Gets the tile info for coordinate.
		/// </summary>
		/// <returns>The tile info for coordinate.</returns>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		public Tile GetTileInfoForCoordinate(int x, int y) {
			if ((x > _worldSize.x) || (y > _worldSize.y))
				throw new UnityException (this + ": have been asked for non existent tile information: " + x + ":" + y);

			return _worldTiles [x, y];
		}

		#endregion

		#region private methods

		/// <summary>
		/// Generates the world model.
		/// </summary>
		private void GenerateWorldModel() {
			if (DebugDefs.kDebugWOrldBuilder)
				Debug.Log ("GenerateWorldModel()");

			for (int w = 0; w < _worldSize.x; w++) {

				for (int y = 0; y < _worldSize.y; y++) {
					_worldTiles[w,y] = new Tile();
				}
			}
		}

		private void CreateTileGameObjects() {
			if (DebugDefs.kDebugWOrldBuilder)
				Debug.Log ("CreateTileGameObjects(): worldSize: " + _worldSize);

			for (int y = 0; y < _worldSize.y; y++) {

				for (int w = 0; w < _worldSize.x; w++) {

					GameObject newObj = new GameObject();
					newObj.name = "tile[ " + w + "," + y + " ]";
					newObj.transform.position = new Vector3(w, 0, y);
					TileObject tile = newObj.AddComponent<TileObject>();
					tile.Init(GetTileInfoForCoordinate(w, y).type, true);

					tile.transform.parent = _world.transform;
				}
			}
		}

		#endregion
	}
}
