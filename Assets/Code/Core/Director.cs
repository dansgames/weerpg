﻿using UnityEngine;
using System.Collections;

using WeeRPG.Core.States;
using WeeRPG.AI;

namespace WeeRPG.Core {

	public class Director : MonoBehaviour 
	{
		#region private fields

		private StateMachine 	_gameStateMachine;
		private WorldBuilder 	_worldBuilder;
		private Character		_player;
		private GameCamera		_camera;

		#endregion

		#region Test Stuff

		private int testX = 10; 
		private int testY = 10;
		private Vector2 testPlayerPos = new Vector2(5, 5);

		#endregion

		void Awake() {
			// Load Save Config
			// HERE

			// Add all components
			InitComponents ();

			// Create "Player"
			InitPlayer ();

			// Set Camera to Player
			_camera.Init (_player);

			_gameStateMachine.ChangeState (new GameState (_player));
		}

		#region private methods

		/// <summary>
		/// Inits the components. Game Machine, World Builder
		/// FUTURE: 
		/// 	Save System etc
		/// </summary>
		private void InitComponents () {
			// Game State Machine
			_gameStateMachine = gameObject.AddComponent<StateMachine> ();
			_gameStateMachine.ChangeState (new BeginState ());
		
			// World
			_worldBuilder = gameObject.AddComponent<WorldBuilder> ();
			_worldBuilder.RunRandomTest (testX, testY);

			// Camera
			_camera = Camera.main.gameObject.AddComponent<GameCamera> ();
		}

		/// <summary>
		/// Inits the player.
		/// FUTURE:
		/// 	Build from saved config
		/// </summary>
		private void InitPlayer () {
			// TODO: TEST
			_player = new Character (testX, testY, testPlayerPos);
		}

		#endregion
	}
}
