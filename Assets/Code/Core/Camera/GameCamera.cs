﻿using UnityEngine;
using System.Collections;

using WeeRPG.Tools;
using WeeRPG.AI;

namespace WeeRPG.Core {

	[RequireComponent(typeof(Camera))]
	public class GameCamera : MonoBehaviour {

		private Camera _camera;
		private Character _player;

		void Awake() {
			_camera = GetComponent<Camera> ();
		}

		public void Init(Character player) {
			_player = player;
		}

		void Update() {
			Vector3 newPos = WorldTools.GetWorldPositionForTileLocation (_player.location);
			newPos.x = newPos.z += 0.5f;
			transform.position = newPos;
		}
	}
}
