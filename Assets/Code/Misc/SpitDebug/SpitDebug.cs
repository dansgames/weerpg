﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;

[ExecuteInEditMode]
public class SpitDebug : MonoBehaviour {

	[SerializeField]
	public object target;

	private List<FieldInfo> _fields = new List<FieldInfo> ();
	public List<FieldInfo> fields { get { return _fields; } }

	private List<PropertyInfo> _props = new List<PropertyInfo> ();
	public List<PropertyInfo> props { get { return _props; } }

	[SerializeField]
	private int _selected;
	public int selected { 
		get { return _selected; }
		set { _selected = value; }
	}

	private string[] _names;
	public string[] names { get { return _names; } }

	void Awake() {
		if(target != null)
			UpdateInfo ();
	}

	void Update() {
		if(target != null)
			UpdateInfo ();

		if (_names != null && _names.Length > _selected) {
			Debug.Log (target.GetType().GetProperty(_names[_selected]).GetValue(target, null));
		}
	}

	void UpdateInfo () {

		target = gameObject;
		
		const BindingFlags flags = /*BindingFlags.NonPublic | */BindingFlags.Public | 
			BindingFlags.Instance | BindingFlags.Static;
		
		_fields = new List<FieldInfo>(target.GetType().GetFields(flags));
		foreach (FieldInfo fieldInfo in fields)
		{
			//Debug.Log("Obj: " + target + ", Field: " + fieldInfo.Name);
		}

		_props = new List<PropertyInfo>(target.GetType().GetProperties(flags));
		foreach (PropertyInfo propertyInfo in props)
		{
			//Debug.Log("Obj: " + target + ", Property: " + propertyInfo.Name);
		}

		_names = new string[_props.Count];
		
		for(int i = 0; i < _props.Count; i++) {
			_names[i] = _props[i].Name;
		}
	}
}
