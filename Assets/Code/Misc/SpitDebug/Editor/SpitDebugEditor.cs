﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Reflection;

[CustomEditor(typeof(SpitDebug))]
public class SpitDebugEditor : Editor 
{
	private PropertyInfo _currentProp;

	public override void OnInspectorGUI()
	{
		SpitDebug myTarget = (SpitDebug)target;

		myTarget.target = EditorGUILayout.TextField (target.ToString());

		myTarget.selected = EditorGUILayout.Popup("Debug Selection", myTarget.selected, myTarget.names); 

//		_currentProp = EditorGUILayout.
//		myTarget.experience = EditorGUILayout.IntField("Experience", myTarget.experience);
//		EditorGUILayout.LabelField("Level", myTarget.Level.ToString());
	}
}