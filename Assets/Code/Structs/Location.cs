﻿public struct Location {
	public int longitude;
	public int lattitude;

	public Location(int inX, int inZ) {
		longitude = inX;
		lattitude = inZ;
	}

	public override string ToString()
	{
		return System.String.Format("{0}:{1}", longitude, lattitude);
	}
}
